class DocumentationController < ActionController::Base

  before_action :generate_docs_in_development

  def index
    render :index, layout: false
  end

  private

  def generate_docs_in_development
    return unless Rails.env.development?
    Swagger::Docs::Generator.write_docs(Swagger::Docs::Config.registered_apis)
  end

end