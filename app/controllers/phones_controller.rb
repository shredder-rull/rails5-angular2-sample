class PhonesController < ApplicationController

  swagger_controller :phones, "Phones"
  register_swagger_model :Phone
  register_swagger_model :PhoneDetails
  register_swagger_model :PhonesResult
  register_swagger_model :Error

  swagger_api :index do
    summary "Load phones"

    param :path, 'brand_id', :string, :required, "Brand"
    param :query, 'page', :integer, :optional, "Page"

    response :ok, 'Success', :PhonesResult
    response :not_found, nil, :Error
  end
  def index
    render json: Phone.all(brand_id: params[:brand_id], page: params[:page])
  end

  swagger_api :show do
    summary "Load phone details"

    param :path, 'id', :string, :required, "Phone ID"

    response :ok, 'Success', :PhoneDetails
    response :not_found, nil, :Error
  end
  def show
    render json: Phone.find(params[:id])
  end

  swagger_api :search do
    summary "Search phones by string"

    param :query, 'query', :string, :optional, "Search phrase"

    response :ok, 'Success', :Phone
    response :not_found, nil, :Error
  end
  def search
    render json: Phone.search(params[:query])
  end

end
