class BrandsController < ApplicationController

  swagger_controller :brands, "Brands"
  register_swagger_model :Brand
  register_swagger_model :Error
  swagger_model :Brand do
    description "Brand"
    property :id, :string, :required, 'ID'
    property :name, :string, :required, 'Name'
  end

  swagger_api :index do
    summary "Load brands"

    response :ok, 'Success', :Brand
    response :not_found, nil, :Error
  end

  def index
    render json: Brand.all
  end

end
