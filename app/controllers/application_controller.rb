class ApplicationController < ActionController::API
  include Swagger::Docs::Methods
  include SwaggerModels
  rescue_from RestClient::NotFound, with: :not_found

  def self.layout(*args); end

  private

  def not_found
    render json: {message: 'Content Not Found - 404', error_code: Api::ERROR_CONTENT_NOT_FOUND}, status: :not_found
  end
  

end
