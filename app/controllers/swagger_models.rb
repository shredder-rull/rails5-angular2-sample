module SwaggerModels
  extend ActiveSupport::Concern

  module ClassMethods

    def register_swagger_model(model)
      case model.to_s.to_sym
      when :Brand then
        swagger_model :Brand do
          description "Brand"
          property :id, :string, :required, 'ID'
          property :name, :string, :required, 'Name'
        end
      when :PhonesResult then
        register_swagger_model :Phone
        swagger_model :PhonesResult do
          description "Phone"
          property :items, :array, :required, 'Phones', :items => {'$ref' => :Phone }
          property :id, :string, :required, 'ID'
          property :name, :string, :required, 'Name'
        end       
      when :Phone then
        swagger_model :Phone do
          description "Phone"
          property :id, :string, :required, 'ID'
          property :name, :string, :required, 'Name'
          property :image_url, :string, :required, 'Image url'
        end
      when :PhoneDetails then
        register_swagger_model :PhoneParamsGroup
        swagger_model :PhoneDetails do
          property :id, :integer, :required, 'Id'
          property :name, :string, :required, 'Name'
          property :image_url, :string, :required, 'Image utl'
          property :details, :array, :required, 'Details', items: {'$ref' => :PhoneParamsGroup }
        end
      when :PhoneParamsGroup then
        register_swagger_model :PhoneParam
        swagger_model :PhoneParamsGroup do
          property :SomeGroup, :array, :required, 'Params', items: {'$ref' => :PhoneParam }
        end
      when :PhoneParam then
        swagger_model :PhoneParam do
          property :name, :string, :required, 'Name'
          property :value, :string, :required, 'Value'
        end
      when  :Error then
        swagger_model :Error do
          description "Error response"
          property :message, :string, :required, "Error Message", {defaultValue: 'Error'}
          property :error_code, :string, :required, "Error Code", {defaultValue: 'Code'}
        end
      else  
        raise "Undefined swagger model: #{model}"
      end

    end  
  end    
end
