class Brand < BaseModel

  def self.all
    Requests::GsmArena::Brands.perform
  end

  def self.as_cache_key
    ['brands']
  end

  class << self
    cache_method :all, 1.day
  end

end