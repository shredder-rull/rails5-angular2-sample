class Phone < BaseModel

  def self.all(params = {})
    Requests::GsmArena::Phones.perform(brand_id: params[:brand_id], page: params[:page])
  end

  def self.find(id)
    Requests::GsmArena::Phone.perform(id: id)
  end

  def self.search(query)
    Requests::GsmArena::Search.perform(query: query)
  end

  def self.as_cache_key
    ['phones']
  end

  class << self
    cache_method :all, 1.hour
    cache_method :find, 1.day
  end

end