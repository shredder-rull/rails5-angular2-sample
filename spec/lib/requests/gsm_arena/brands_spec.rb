require 'rails_helper'

RSpec.describe Requests::GsmArena::Brands do

  it 'returns models list from gsmarena' do
    results = Requests::GsmArena::Brands.perform
    expect(results.count).to be > 0
    expect(results).to all(match({:name => a_kind_of(String), :id => a_kind_of(String)}))
  end

end