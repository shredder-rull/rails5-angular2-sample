require 'rails_helper'

RSpec.describe Requests::GsmArena::Phones do

  it 'returns phones list from gsmarena by model_id' do
    brands = Requests::GsmArena::Brands.perform
    samsung = brands.find{|m| m[:name] == 'Samsung'}[:id] rescue 'samsung-phones-9'
    result = Requests::GsmArena::Phones.perform(:brand_id => samsung)
    expect(result).to match({
      pages: {count: a_kind_of(Integer), current: a_kind_of(Integer)},
      items: a_kind_of(Array)
    })
    expect(result[:items].count).to be > 0
    expect(result[:items]).to all(match({
      :name => a_kind_of(String),
      :id => a_kind_of(String),
      :image_url => a_kind_of(String)
    }))
  end

end