
require 'rails_helper'

RSpec.describe Requests::GsmArena::Phone do

  let(:phone_id){ 'samsung_galaxy_j-5887' }

  it 'returns phone details' do
    phone = Requests::GsmArena::Phone.perform(:id => phone_id)
    expect(phone).to a_kind_of(Hash)
    expect(phone).to match({
      id: a_kind_of(String),
      name: a_kind_of(String),
      image_url: a_kind_of(String),
      details: a_kind_of(Hash)
    })
    expect(phone[:details].keys).to eq(["Network", "Launch", "Body", "Display", "Platform", "Memory", "Camera", "Sound", "Comms", "Features", "Battery", "Misc"])
    expect(phone[:details].values.flatten).to all(match({:name => a_kind_of(String), :value => a_kind_of(String)}))
  end

end