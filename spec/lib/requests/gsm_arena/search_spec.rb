require 'rails_helper'

RSpec.describe Requests::GsmArena::Search do

  let(:query){ 'Samsung' }

  it 'returns phones list from gsmarena by model_id' do
    phones = Requests::GsmArena::Search.perform(:query => query)
    expect(phones.count).to be > 0
    expect(phones).to all(match({
      :name => match(/#{query}/),
      :id => a_kind_of(String),
      :image_url => a_kind_of(String)
    }))
  end

end