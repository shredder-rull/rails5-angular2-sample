require 'rails_helper'

RSpec.describe Requests::GsmArena::Base do
  
  it 'has correct default options' do
    expect(subject.default_options).to match({
      method: :get,
      url: 'http://www.gsmarena.com'
    })
  end

end