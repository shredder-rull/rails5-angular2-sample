require 'rails_helper'

RSpec.describe Requests::Base do


  let :test_class do
    Class.new(Requests::Base) do
      url 'http://www.test.com'
      type :post
      headers({'Content-Type' => 'text/html'})
    end
  end

  let :test_options do
    test_class.default_options.dup
  end

  it 'set default options to class' do
    expect(test_class.default_options).to match(test_options)
  end

  it 'set default options to instance' do
    expect(test_class.new.default_options).to match(test_options)
  end

  it 'child class inherits default options' do
    test_subclass = Class.new(test_class)
    expect(test_subclass.default_options).to match(test_options)
    expect(test_subclass.new.default_options).to match(test_options)
  end

  it 'change options in inherited classes merges with parent options' do
    test_subclass = Class.new(test_class) do
      url 'https://www.github.com'
      type :patch
    end

    expect(test_subclass.default_options).to match(test_options.merge({
      url: 'https://www.github.com',
      method: :patch,
    }))
  end

  it 'change options in inherited classes does not change parent options' do
    test_subclass = Class.new(test_class) do
      url 'https://www.github.com'
      type :patch
    end

    expect(test_class.default_options).to match(test_options)
  end

end