module ResponseHelper

  def json_response
    return @json_response if response.body.object_id == @response_id
    @response_id, @json_response = response.body.object_id, wrap_hashie(MultiJson.load(response.body))
    @json_response
  # rescue MultiJson::ParseError => e
  #   binding.pry
  end

  private

  def wrap_hashie(subject)
    subject.is_a?(Array) ? subject.map{|item| Hashie::Mash.new(item)} : Hashie::Mash.new(subject)
  end

end