require 'rails_helper'

RSpec.describe BrandsController, type: :controller do

  it 'returns brands from gsmarena' do
    get :index
    expect(response).to have_http_status(:success)
    expect(json_response.count).to be > 0
    expect(json_response).to all(match({:name => a_kind_of(String), :id => a_kind_of(String)}))
  end

end
