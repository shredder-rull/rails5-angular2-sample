require 'rails_helper'

RSpec.describe PhonesController, type: :controller do

  let(:phone_id){ 'samsung_galaxy_j_max-8186' }
  let(:brand_id){ 'samsung-phones-9' }
  let(:query){ 'Samsung' }

  it 'returns phones by brand_id from gsmarena' do
    get :index, params: {brand_id: brand_id}
    expect(response).to have_http_status(:success)

    expect(json_response).to match({
      pages: {count: a_kind_of(Integer), current: a_kind_of(Integer)},
      items: a_kind_of(Array)
    })
    expect(json_response[:items].count).to be > 0
    expect(json_response[:items]).to all(match({
      :name => a_kind_of(String),
      :id => a_kind_of(String),
      :image_url => a_kind_of(String)
    }))

  end

  it 'returns phone by id' do
    get :show, params: {id: phone_id}
    expect(response).to have_http_status(:success)
    expect(json_response).to a_kind_of(Hash)
    expect(json_response).to match({
      id: a_kind_of(String),
      name: a_kind_of(String),
      image_url: a_kind_of(String),
      details: a_kind_of(Hash)
    })
    expect(json_response[:details].keys).to eq(["Network", "Launch", "Body", "Display", "Platform", "Memory", "Camera", "Sound", "Comms", "Features", "Battery", "Misc"])
    expect(json_response[:details].values.flatten).to all(match({:name => a_kind_of(String), :value => a_kind_of(String)}))
  end

  it 'searches phones by query' do
    get :search, params: {query: query}
    expect(response).to have_http_status(:success)
    expect(json_response.count).to be > 0
    expect(json_response).to all(match({
      :name => match(/#{query}/),
      :id => a_kind_of(String),
      :image_url => a_kind_of(String)
    }))
  end

end
