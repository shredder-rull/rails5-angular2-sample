import { Component, OnInit, OnDestroy } from '@angular/core';
import { Phone, Page } from '../../models';
import { SearchComponent } from '../search/search.component';
import { PhonesComponent } from '../phones/phones.component';
import { PhoneService } from '../../services';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'dashboard',
  styleUrls: ['./dashboard.component.css'],
  templateUrl: './dashboard.component.html',
  directives: [SearchComponent, PhonesComponent],
  providers: [PhoneService]
})

export class DashboardComponent implements OnInit, OnDestroy {
  phones: Phone[] = [];
  pageParams: Page;
  private sub: any; 

  constructor(
    private route: ActivatedRoute,
    private phoneService: PhoneService){}

  ngOnInit(){
    this.sub = this.route.params.subscribe(params => {
      let page = parseInt(params['page'] || '1');
      if (params['brand_id']) this.selectBrand(params['brand_id'], page);
      if (params['query']) this.searchPhones(params['query']);
    });
  }

  selectBrand(brand_id: string, page?: number) {
    this.phoneService.getPhones(brand_id, page).then(result => {
      this.pageParams = result.pages.count > 1 ? result.pages : null
      this.phones = result.items
    });   
  }

  searchPhones(query: string) {
    this.phoneService.searchPhones(query).then(phones => {
      this.pageParams = null;
      this.phones = phones
    }); 
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}