import { Component } from '@angular/core';
//import { HeroService } from './heroes/hero.service'
//import { HeroesComponent } from './heroes/heroes.component'
//import { HeroDetailComponent } from './heroes/hero-detail.component'
import { DashboardComponent, PhoneDetailsComponent } from '../../components';
import { ROUTER_DIRECTIVES } from '@angular/router';
//import './rxjs/rxjs-operators';

@Component({
  selector: 'tz-app',
  directives: [ ROUTER_DIRECTIVES ],
  styleUrls: ['./app.component.css'],
  templateUrl: './app.component.html',
  precompile: [ DashboardComponent, PhoneDetailsComponent ]
})

export class AppComponent { }