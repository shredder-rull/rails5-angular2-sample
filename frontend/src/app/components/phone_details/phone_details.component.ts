import { Component, OnInit, OnDestroy } from '@angular/core';
import { Phone } from '../../models';
import { PhoneService } from '../../services';
import { Router, ActivatedRoute } from '@angular/router';
import { ValuesPipe, KeysPipe } from '../../pipes';

@Component({
  selector: 'phone-details',
  styleUrls: ['./phone_details.component.css'],
  templateUrl: './phone_details.component.html',
  providers: [PhoneService],
  pipes: [ValuesPipe, KeysPipe]
})

export class PhoneDetailsComponent implements OnInit, OnDestroy {

  phone: Phone;
  id: string;
  private sub: any;

  constructor(
    private phoneService: PhoneService,
    private route: ActivatedRoute,
    private router: Router) {}

  ngOnInit(){
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
      this.getPhone();
    })
  }

  getPhone(){
    this.phoneService.getPhone(this.id).then(phone => this.phone = phone);
  }

  ngOnDestroy(){
    this.sub.unsubscribe();
  }

}