import { Component, Input, Output, OnInit, EventEmitter, OnDestroy } from '@angular/core';
import { Brand, Phone } from '../../models';
import { BrandService } from '../../services';
import { ROUTER_DIRECTIVES } from '@angular/router';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'search',
  styleUrls: ['./search.component.css'],
  templateUrl: './search.component.html',
  providers: [BrandService],
  directives: [ROUTER_DIRECTIVES]
})

export class SearchComponent implements OnInit, OnDestroy {
  @Input() phones: Phone[];
  brands: Brand[];
  private sub: any;
  brand_id: string;
  query: string = '';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private brandService: BrandService ){}

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.brand_id = params['brand_id'];
      this.query = params['query'] || '';
      this.getBrands();
    });
  }

  getBrands() {
    this.brandService.getBrands().then(brands => this.brands = brands);
  }

  chooseBrand(brand_id: string) {
    this.router.navigate(['brands', brand_id]);
  }

  searchPhones(query: string) {
    this.router.navigate(['phones/search', {query: query}]);
  }

  gotoPhone(phone_id: string) {
    this.router.navigate(['phones', phone_id]); 
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}