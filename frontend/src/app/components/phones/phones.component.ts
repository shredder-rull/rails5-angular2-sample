import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Phone, Page } from '../../models';
import { ROUTER_DIRECTIVES, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'phones',
  styleUrls: ['./phones.component.css'],
  templateUrl: './phones.component.html',
  directives: [ ROUTER_DIRECTIVES ]
})

export class PhonesComponent implements OnInit {
  @Input() phones: Phone[];
  @Input() pageParams: Page;
  brandId: String;
  private sub: any;

  constructor(private route: ActivatedRoute){}

  ngOnInit(){
    this.sub = this.route.params.subscribe(params =>{
      this.brandId = params['brand_id'];
    });
  }

  pages(){
    if (this.pageParams) {
      let count = parseInt(this.pageParams.count);
      var pages:Number[] = [];
      for (var p = 1; p <= count; p++ ) { pages.push(p); }
      return pages;
    }
    return [];
  }

  ngOnDestroy(){

  }
}