export class Phone {
  id: string;
  name: string;
  image_url: string;
  details: any = null;
}