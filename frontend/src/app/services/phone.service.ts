import { Injectable } from '@angular/core';
import { Http, Response, URLSearchParams } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { Phone } from '../models';

const API_URI = process.env.API_URI;

@Injectable()
export class PhoneService {

  constructor (private http: Http) {}

  getPhones(brand_id: string, page?: number): Promise<any> {
    let params: URLSearchParams = new URLSearchParams();
    params.set('page', page.toString());
    return this.http.get(`${API_URI}/brands/${brand_id}/phones`, {search: params})
                .toPromise()
                .then(this.extractData)
                .catch(this.handleError);
  }

  searchPhones(query: string): Promise<Phone[]> {
    let params: URLSearchParams = new URLSearchParams();
    params.set('query', query);

    return this.http.get(`${API_URI}/phones/search`, {search: params})
                .toPromise()
                .then(this.extractData)
                .catch(this.handleError);
  }

  getPhone(id: string): Promise<any>{
    return this.http.get(`${API_URI}/phones/${id}`)
                .toPromise()
                .then(this.extractData)
                .catch(this.handleError);
  }

  private extractData(res: Response) {
    return res.json();
  }

  private handleError (error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }
}
