import { provideRouter, RouterConfig }  from '@angular/router';
import { DashboardComponent, PhoneDetailsComponent } from './components';

const routes: RouterConfig = [
  { path: '', component: DashboardComponent },
  { path: 'brands/:brand_id', component: DashboardComponent },
  { path: 'phones/search', component: DashboardComponent },
  { path: 'phones/:id', component: PhoneDetailsComponent },
];

export const APP_ROUTER_PROVIDERS = [
  provideRouter(routes)
];