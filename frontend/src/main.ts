import { bootstrap }    from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';
import { AppComponent } from './app/components/app/app.component';
import { APP_ROUTER_PROVIDERS } from './app/app.router';
import { HTTP_PROVIDERS } from '@angular/http';
// import { ROUTER_PROVIDERS } from '@angular/router';
// import { HTTP_PROVIDERS } from '@angular/http';


//console.log(process.env.API_URI);

if (process.env.ENV === 'production') {
  enableProdMode();
}

//bootstrap(AppComponent, [ ROUTER_PROVIDERS, HTTP_PROVIDERS ]);
bootstrap(AppComponent, [ APP_ROUTER_PROVIDERS, HTTP_PROVIDERS ]);