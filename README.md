# Angular2 Rails Example

Angular2 + Rails 5 + Webpack examples.

## Install

You need to have [node](https://nodejs.org/en/) installed on your system. If not, head over to [https://nodejs.org/en/](https://nodejs.org/en/) and install the right version for your OS.

1. Clone the repo locally on your machine.
2. Go to root directory `cd rails5-angular2-sample`.
3. Run `npm install`.
4. Run `bundle install`

## Running

npm start

bundle exec rails server

Open http://localhost:8080 with your browser
