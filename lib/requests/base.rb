##
# Base HTTP request
module Requests
  class Base

    class ParamsError < RuntimeError; end

    class_attribute :default_options
    self.default_options = {}

    attr_reader :params

    ##
    # Constructor
    # Takes params Return than uses as
    # query params by default
    def initialize(params = {})
      @params = params
    end

    ##
    # Performs requests with RestClient::Request
    # Resturns response body. Does not parse by default
    def perform
      body
    end

    ##
    # Instantiates new request and perform it
    def self.perform(params = {})
      self.new(params).perform
    end

    protected

    ##
    # Allows setting url in one line.
    #
    #   class Foo < Requests::Base
    #     url 'http://www.test.com'
    #   end
    def self.url(url = nil)
      url ? set_option(:url, url) : default_options[:url]
    end

    ##
    # Allows setting request method (:get, :post, :put, :patch, :post). 
    #
    #   class Foo < Requests::Base
    #     type :post
    #   end
    def self.type(type = nil)
      type ? set_option(:method, type) : default_options[:method]
    end

    ##
    # Allows setting header. 
    #
    #   class Foo < Requests::Base
    #     type :headers, {'Content-Type' => 'text/html'}
    #   end
    def self.headers(headers = {})
      headers ? set_option(:headers, headers) : default_options[:headers]
    end

    ##
    # Allows set any option safe
    def self.set_option(option, value)
      self.default_options = self.default_options.merge({option => value}.symbolize_keys)
      value
    end

    private

    ##
    # Returns options hash for RestClient::Request
    def request_options
      default_options.dup.tap do |opts|
        opts[:headers] ||= {}
        opts[:headers].merge!({params: query_params}) if query_params.present?
        opts[:payload] = payload if payload.present? and [:post, :put, :patch].include?(opts[:method].to_sym)
      end
    end

    ##
    # Returns query options for RestClient::Request
    # You can override in subclasses
    #
    #   class Foo < Requests::Base
    #     def query_params
    #       {page: params[:page]}
    #     end
    #   end
    def query_params
      params
    end

    ##
    # Returns body for post, put, patch requests
    # Uses params by default
    def payload
      params
    end

    ##
    # Executes request and return responce
    def response
      @response ||= RestClient::Request.execute(request_options)
    end

    ##
    # Wrapper for response.body
    def body
      response.body
    end

  end
end