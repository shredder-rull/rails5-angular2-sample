##
# Phones request
# Response:
# {
#   items: [
#     {
#       id: String,
#       name: String,
#       image_url: String
#     }
#   ],
#   pages: {
#     count: Integer,
#     current: Integer
#   }
#} 
module Requests
  module GsmArena
    class Phones < Requests::GsmArena::Base

      root_path '.main.main-maker'

      has_many :items, '.makers li' do
        has_one :name, 'a'
        has_one :id, nil, ->(id){ node_to_string(id).sub(/\.php$/, '') }, xpath: 'a/@href'
        has_one :image_url, :xpath => 'a/img/@src'
      end

      has_one :page, '.nav-pages strong', Integer
      has_many :pages, '.nav-pages a', Integer

      def perform
        raise ParamsError, 'Parameter "brand_id" is missing' if params[:brand_id].blank?
        super
      end

      def request_options
        url = "#{default_options[:url]}/#{params[:brand_id]}.php"
        # For perform request with page we need change end of path
        # Example: '...-59.php' => '...-f-59-0p{page}'
        url.gsub!(/(\d+)\.php/, "f-\\1-0-p#{page}.php") if page > 1
        super.merge url: url
      end

      private

      def page
        params[:page].to_i
      end

      ##
      # Processes response after parsing
      # Sets page params to parsing result
      def after_parse(result)
        result[:pages] = {
          count: result.delete(:pages).max || 1,
          current: result.delete(:page) || 1
        }
        result
      end

    end
  end
end