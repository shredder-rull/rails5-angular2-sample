##
# Brands request
# Response:
# [
#   {
#     id: String,
#     name: String,
#   }
# ]
#
module Requests
  module GsmArena
    class Brands < Requests::GsmArena::Base

      root_path '.brandmenu-v2 li'
      few true

      has_one :name, 'a'
      has_one :id, nil, ->(id){ node_to_string(id).sub(/\.php$/, '') }, xpath: 'a/@href'

    end
  end
end