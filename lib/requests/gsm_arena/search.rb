##
# Search request
# Response:
# [
#   {
#     id: String,
#     name: String,
#   }
# ]
#

module Requests
  module GsmArena
    class Search < Requests::GsmArena::Base

      url 'http://www.gsmarena.com/results.php3'

      root_path '.makers li'
      few true

      has_one :name, 'a'
      has_one :id, nil, ->(id){ node_to_string(id).sub(/\.php$/, '') }, xpath: 'a/@href'
      has_one :image_url, :xpath => 'a/img/@src'

      def query_params
        {
          'sQuickSearch' => 'yes',
          'sName' => params[:query]
        }
      end

      def perform
        super
      end

    end
  end
end