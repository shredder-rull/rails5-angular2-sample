##
# Phone details request
# Response:
# {
#   id: String,
#   name: String,
#   image_url: String,
#   details: {
#     'SomeGroup': [
#       {
#         name: String,
#         value: String
#       }
#     ]
#   }
# }
module Requests
  module GsmArena
    class Phone < Requests::GsmArena::Base

      root_path '.main.main-review'

      has_one :name, '.specs-phone-name-title'
      has_one :image_url, :xpath => ".//*[@class='specs-photo-main']/a/img/@src"

      has_many :details, '#specs-list table tr' do
        has_one :category, 'th'
        has_one :name, 'td.ttl'
        has_one :value, 'td.nfo'
      end

      def request_options
        super.merge url: "#{default_options[:url]}/#{params[:id]}.php"
      end

      def perform
        raise ParamsError, 'Parameter "id" is missing' if params[:id].blank?
        super
      end

      private

      def after_parse(result)
        result[:id] = params[:id]
        st = nil
        result[:details] = result[:details].each_with_object({}) do |p, obj|
          st = p[:category] if p[:category]
          obj[st] ||= []
          obj[st] << p.slice(:name, :value)
        end
        result
      end

    end
  end
end