##
# Basic request for www.gsmarena.com
module Requests
  module GsmArena
    class Base < Requests::Base
      include DslParsers::NokogiriHtmlParser

      type :get
      url 'http://www.gsmarena.com'

      ##
      # Parses requests with HtmlParser
      # Parser details define in subclasses
      def perform
        parse(body)
      end

    end
  end
end