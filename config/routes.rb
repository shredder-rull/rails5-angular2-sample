Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  scope :api, format: :json do
    get 'docs' => 'documentation#index', format: 'html'
    resources :brands, only: [:index] do
      resources :phones, only: [:index]
    end
    resources :phones, only: [:show] do
      get :search, on: :collection
    end
  end
end
