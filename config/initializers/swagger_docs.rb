Swagger::Docs::Config.class_eval do

  register_apis({
      "1.0" => {
          # the extension used for the API
          :api_extension_type => :json,
          # the output location where your .json files are written to
          :api_file_path => 'public/apidocs',
          # the URL base path to your API
          :base_path => Rails.env.production? ? "https://agile-plateau-50217.herokuapp.com/" : 'http://localhost:3000',

          :camelize_model_properties => false,

          :parent_controller => ActionController::API
      }
  })

  def self.transform_path(path, api_version)
    "apidocs/#{path}"
  end

end

# module Swagger
#   module Docs
#     class Generator

#       def self.transform_spec_to_api_path(spec, controller_base_path, extension)
#         api_path = spec.to_s.dup
#         api_path.gsub!('(.:format)', extension ? ".#{extension}" : '')
#         api_path.gsub!(/:(\w+)/, '{\1}')
#         api_path.gsub!(controller_base_path, '')
#         api_path.gsub!(/\((.+)\)/, '\1')
#         "/api/" + trim_slashes(api_path)        
#       end
#     end
#   end
# end
